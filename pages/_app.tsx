import { CssBaseline, Stack, ThemeProvider } from "@mui/material";
import { createTheme } from "@mui/material/styles";
import type { AppProps } from "next/app";
import Head from "next/head";
import "regenerator-runtime/runtime";
import Footer from "../src/components/pages/index/Footer";
import { themeOptions } from "../src/theme";
import "../styles/globals.css";

const theme = createTheme(themeOptions);

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>RUNIE</title>
        <meta name="description" content="Mode to earn, make life better!" />
        <link rel="icon" href="/images/Logo/Asset 9@3x.png" />
      </Head>
      <ThemeProvider theme={theme}>
        <CssBaseline />

        <Stack width="100%" height="100%">
          <Component {...pageProps} />
          <Footer />
        </Stack>
      </ThemeProvider>
    </>
  );
}

export default MyApp;
