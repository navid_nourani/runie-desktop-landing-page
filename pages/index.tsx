import Stack from "@mui/material/Stack";
import type { NextPage } from "next";
import Head from "next/head";
import Advisors from "../src/components/pages/index/Advisors";
import DescriptionBanner from "../src/components/pages/index/DescriptionBanner";
import DownloadApps from "../src/components/pages/index/DownloadApps";
import FoundationStory from "../src/components/pages/index/FoundationStory";
import HowToEarn from "../src/components/pages/index/HowToEarn";
import Investors from "../src/components/pages/index/Investors";
import MainBanner from "../src/components/pages/index/MainBanner";
import NFTSneakers from "../src/components/pages/index/NFTSneakers";
import Roadmap from "../src/components/pages/index/Roadmap";
import RONTokenData from "../src/components/pages/index/RONTokenData";
import TheTeam from "../src/components/pages/index/TheTeam";
import Tokenomic from "../src/components/pages/index/Tokenomic";
import WhatIsSpecial from "../src/components/pages/index/WhatIsSpecial";

const Home: NextPage = () => {
  return (
    <Stack overflow={"hidden"}>
      <Head>
        <title>RUNIE</title>
        <meta name="description" content="Mode to earn, make life better!" />
        <link rel="icon" href="/images/Logo/Asset 9@3x.png" />
      </Head>
      <MainBanner />
      <DescriptionBanner />
      <DownloadApps />
      <HowToEarn />
      <WhatIsSpecial />
      <Tokenomic />
      <RONTokenData />

      <NFTSneakers />
      <Roadmap />
      <Advisors />
      <TheTeam />
      <FoundationStory />
      <Investors />
    </Stack>
  );
};

export default Home;
