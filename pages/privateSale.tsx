import { Box, createTheme, Stack, styled, ThemeProvider } from "@mui/material";
import AirdropResult from "components/organism/AirdropResult";
import Buy from "components/pages/private-sale/Buy";
import { useUpdateAtom } from "jotai/utils";
import Image from "next/image";
import React, { useEffect } from "react";
import MainBanner from "../src/components/pages/index/MainBanner";
import { bnbPriceAtom } from "../src/stores/pages/priateSale";
import { themeOptions } from "../src/theme";

const theme = createTheme({
  ...themeOptions,
  palette: {
    primary: { main: "#375A64", contrastText: "#FCF3F3" },
    secondary: { main: "#C69C6D", contrastText: "#FCF3F3" },
  },
  typography: {
    fontFamily: "Tw Cen MT",
  },
  components: {
    MuiTypography: {
      styleOverrides: {
        root: {
          color: "white",
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          borderRadius: "35px",
        },
      },
    },
    MuiInput: {
      defaultProps: {
        disableUnderline: true,
      },
      styleOverrides: {
        root: {
          paddingInline: "12px",
          backgroundColor: "#FAFEFF",
          textDecorationLine: "none",
          borderRadius: "35px",
        },
      },
    },
  },
});
const PrivatreSell = () => {
  const setBNBPrice = useUpdateAtom(bnbPriceAtom);

  const getBNBPrice = async () => {
    while (true) {
      const res = await fetch(
        "https://api.binance.com/api/v3/ticker/price?symbol=BTCUSDT"
      );
      if (res.status === 200) {
        const data = await res.json();
        setBNBPrice(data.price);
        break;
      }
    }
  };

  useEffect(() => {
    getBNBPrice();
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <Box
        py="105px"
        zIndex="5"
        position="relative"
        width="100%"
        minHeight="100vh"
        justifyContent="center"
        alignItems="center"
        display="flex"
      >
        <Box
          position="absolute"
          width="100%"
          height="100%"
          sx={{ "&>*": { height: "100% !important" } }}
        >
          <MainBanner />
        </Box>
        <Stack position="relative">
          <AirdropResult />
          <Buy
            allocation="16,000,000"
            hardCap={"2000"}
            runiePriceInDollars={0.05}
            bnbMinBuy={0.125}
            title="Private sale"
            decimalNumberOfDigits={6}
            distributionDate={"June 2nd 2022"}
            closeTime="23:30 PM (UTC) May 3rd 2022"
            duration="May 1st - May 3rd 2022"
            closeDescription="The private sale will close before the due date if the allocation is sold out!"
            runieInBnbPrice="0.000125"
          />
          <GiveAwayContainer>
            <Box
              position="relative"
              sx={{
                aspectRatio: "709/359",
              }}
            >
              <Image
                src="/images/Giveaway@3x.png"
                alt=""
                layout="fill"
                objectFit="contain"
              />
            </Box>
          </GiveAwayContainer>
        </Stack>
      </Box>
    </ThemeProvider>
  );
};

const GiveAwayContainer = styled(Box)`
  background: radial-gradient(
      98.19% 451.52% at 93.18% 32.96%,
      #375a64 50.54%,
      #318b8b 96.88%
    )
    /* warning: gradient uses a rotation that is not supported by CSS and may not behave as expected */;
  border: 3px solid #fffbfb;
  box-sizing: border-box;
  box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.25);
  border-radius: 35px;
  padding: 20px;
  overflow: hidden;
  margin-block: 15px;
`;

export default PrivatreSell;
