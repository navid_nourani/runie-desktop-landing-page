import { NextApiRequest, NextApiResponse } from "next";
import mysql from "serverless-mysql";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  console.log({
    host: process.env.NEXT_PUBLIC_host,
    port: (process.env.NEXT_PUBLIC_port ?? 3306) as Number,
    user: process.env.NEXT_PUBLIC_user,
    password: process.env.NEXT_PUBLIC_password,
    database: process.env.NEXT_PUBLIC_database,
  });
  var db = mysql({
    // config: {
    //   host: "5.172.177.217",
    //   port: 3306,
    //   user: "Runie_user_1",
    //   password: "tF37ay2_1",
    //   database: "artimant_Runie",
    // },
    config: {
      host: process.env.NEXT_PUBLIC_host,
      port: Number(process.env.NEXT_PUBLIC_port),
      user: process.env.NEXT_PUBLIC_user,
      password: process.env.NEXT_PUBLIC_password,
      database: process.env.NEXT_PUBLIC_database,
    },
  });

  const walletAddress = req.query.walletAddress as string;

  const data: any = await db.query(
    `SELECT * FROM user where Details = '${walletAddress}'`
  );
  await db.end();

  // const prisma = new PrismaClient();
  // const data = await prisma.user.findFirst({
  //   where: { Details: walletAddress },
  // });
  if (data) {
    res.status(200).json(data[0]);
  } else {
    res.status(404).json({
      message: "User not found",
    });
  }
}
