import { Box, createTheme, Stack, ThemeProvider } from "@mui/material";
import AirdropResult from "components/organism/AirdropResult";
import Buy from "components/pages/private-sale/Buy";
import { useUpdateAtom } from "jotai/utils";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import MainBanner from "../src/components/pages/index/MainBanner";
import { bnbPriceAtom } from "../src/stores/pages/priateSale";
import { themeOptions } from "../src/theme";

const theme = createTheme({
  ...themeOptions,
  palette: {
    primary: { main: "#375A64", contrastText: "#FCF3F3" },
    secondary: { main: "#C69C6D", contrastText: "#FCF3F3" },
  },
  typography: {
    fontFamily: "Tw Cen MT",
  },
  components: {
    MuiTypography: {
      styleOverrides: {
        root: {
          color: "white",
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          borderRadius: "35px",
        },
      },
    },
    MuiInput: {
      defaultProps: {
        disableUnderline: true,
      },
      styleOverrides: {
        root: {
          paddingInline: "12px",
          backgroundColor: "#FAFEFF",
          textDecorationLine: "none",
          borderRadius: "35px",
        },
      },
    },
  },
});

const PrivatreSell: NextPage = () => {
  const router = useRouter();
  const setBNBPrice = useUpdateAtom(bnbPriceAtom);

  const getBNBPrice = async () => {
    while (true) {
      const res = await fetch(
        "https://api.binance.com/api/v3/ticker/price?symbol=BTCUSDT"
      );
      if (res.status === 200) {
        const data = await res.json();
        setBNBPrice(data.price);
        break;
      }
    }
  };

  useEffect(() => {
    getBNBPrice();
  }, []);

  // Remove this lines when PRESALE opened
  //******************************* */
  useEffect(() => {
    router.replace("/");
  }, [router]);

  return null;
  //******************************* */

  return (
    <ThemeProvider theme={theme}>
      <Box position={"relative"} height="100%">
        <Box
          position="absolute"
          width="100%"
          minHeight="100vh"
          height="100%"
          sx={{ "&>*": { height: "100% !important" } }}
        >
          <MainBanner />
        </Box>
        <Box
          position="relative"
          width="100%"
          minHeight="100vh"
          justifyContent="center"
          alignItems="center"
          display="flex"
        >
          <Stack position="relative" my="105px">
            <AirdropResult />
            <Buy
              allocation="20,000,000"
              hardCap="5000"
              runiePriceInDollars={0.1}
              title="Presale"
              decimalNumberOfDigits={5}
              distributionDate={"May 27th 2022"}
              closeTime="23:30 PM (UTC) May 15th 2022"
              duration="May 5th - May 15th 2022"
              closeDescription="The presale will close before the due date if the allocation is sold out!"
              runieInBnbPrice="0.00025"
            />
          </Stack>
        </Box>
      </Box>
    </ThemeProvider>
  );
};

export default PrivatreSell;
