import { ImageLoaderProps } from "next/image";

export const imageLoader = ({ src, width, quality }: ImageLoaderProps) => {
  return `https://runie.vercel.app/_next/image?url=${src}&w=${width}&q=${
    quality || 75
  }`;
};
