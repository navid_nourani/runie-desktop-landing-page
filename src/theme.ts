import { ThemeOptions } from "@mui/material";

export const themeOptions: ThemeOptions = {
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1235 + 48,
      xl: 1536,
    },
  },
  palette: {
    primary: {
      main: "#375A64",
    },
    background: {
      default: "#E5E5E5",
    },
  },
  typography: {
    fontFamily: "Tw Cen MT",
    fontWeightBold: "900",
    fontWeightLight: "700",
    fontWeightMedium: "700",
    fontWeightRegular: "700",
  },
};
