// import { user } from "@prisma/client";
import { atom } from "jotai";

export interface User {
  Entry: number;
  Full_Name: string;
  Total_Entries: string;
  Details: string;
  Whitelist: string;
  Email_Address: string;
  Country?: string;
}

export const airDropDataAtom = atom<User | undefined>(undefined);
