import { atom } from "jotai";

export const metamaskWalletAddressAtom = atom("");
export const bnbAmountAtom = atom("0.125");
export const bnbPriceAtom = atom("");
