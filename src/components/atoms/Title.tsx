import styled from "@emotion/styled";
import { Typography } from "@mui/material";

export const Title = styled(Typography)`
  font-style: normal;
  font-weight: 700;
  font-size: 52px;
  line-height: 78px;
  /* identical to box height */

  text-transform: capitalize;
`;
