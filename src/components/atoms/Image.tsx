import NextImage, { ImageProps } from "next/image";
import React, { FunctionComponent } from "react";
type Props = ImageProps;

const Image: FunctionComponent<Props> = (props) => {
  // return <NextImage {...props} loader={imageLoader} />;
  return <NextImage {...props} />;
};

export default Image;
