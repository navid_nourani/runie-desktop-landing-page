import { styled } from "@mui/material";

export const Divider = styled("div")`
  width: 2px;
  height: 100%;
  background-color: white;
  border-radius: 10px;
`;
