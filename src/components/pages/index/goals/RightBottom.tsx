import { Stack, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import { styled } from "@mui/system";
import DbSVG from "components/svg/goals/Profit.svg";
import React from "react";
import RightTopSvg from "../../../svg/goals/Group 16RightBottom.svg";
const RightBottom = () => {
  return (
    <Box position="absolute" right="0" bottom="28%" maxWidth="34%">
      <Box position="relative">
        <Stack
          marginLeft="37px"
          direction="row"
          alignItems="center"
          paddingBottom="15px"
        >
          <Box flexShrink={0}>
            <DbSVG />
          </Box>
          <Description marginLeft="18px">
            {`RUNIE's token burning mechanisms have
          been carefully prepared and calculated
          to help the token grow well along the
          long-term roadmap.`}
          </Description>
        </Stack>
        <Box position="absolute" bottom={0} sx={{ "*": { width: "100%" } }}>
          <RightTopSvg />
        </Box>
      </Box>
    </Box>
  );
};

const Description = styled(Typography)`
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 21px;

  color: #ffffff;
`;

export default RightBottom;
