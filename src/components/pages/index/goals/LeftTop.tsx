import styled from "@emotion/styled";
import { Stack, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import MoneySVG from "components/svg/goals/Money.svg";
import React from "react";
import LeftTopSvg from "../../../svg/goals/Group 12LeftTop.svg";

const LeftTop = () => {
  return (
    <Box position="absolute" left="0" bottom="85%" maxWidth="34%">
      <Box position="relative">
        <Stack direction="row" alignItems={"center"} paddingBottom="10px">
          <Box flexShrink={0}>
            <MoneySVG />
          </Box>
          <Description marginLeft="18px">
            {`The Move To Earn mechanism that RUNIE is working on currently being warmly
            received by the community and creating
            a new trend.`}
          </Description>
        </Stack>
        <Box
          position="absolute"
          bottom={"-55px"}
          sx={{ "*": { width: "100%" } }}
        >
          <LeftTopSvg />
        </Box>
      </Box>
    </Box>
  );
};

const Description = styled(Typography)`
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 21px;

  color: #ffffff;
`;

export default LeftTop;
