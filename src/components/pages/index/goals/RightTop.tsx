import styled from "@emotion/styled";
import { Stack, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import BarChartSVG from "components/svg/goals/BarChart.svg";
import React from "react";
import RightTopSvg from "../../../svg/goals/Group 11RightTop.svg";

const RightTop = () => {
  return (
    <Box position="absolute" right="0" bottom="85%" maxWidth="34%">
      <Box position="relative">
        <Stack
          marginLeft="37px"
          paddingBottom="10px"
          direction="row"
          alignItems={"center"}
        >
          <Box flexShrink={0}>
            <BarChartSVG />
          </Box>
          <Description marginLeft="18px">
            {`The exercises and health textbooks
            that RUNIE brings are thorough advice
            from experienced health experts.`}
          </Description>
        </Stack>
        <Box
          position="absolute"
          bottom={"-55px"}
          sx={{ "*": { width: "100%" } }}
        >
          <RightTopSvg />
        </Box>
      </Box>
    </Box>
  );
};

const Description = styled(Typography)`
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 21px;

  color: #ffffff;
`;

export default RightTop;
