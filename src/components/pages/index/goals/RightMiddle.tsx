import styled from "@emotion/styled";
import { Stack, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import FlagSVG from "components/svg/goals/Flag.svg";
import React from "react";
import RightMiddleSvg from "../../../svg/goals/Group 17RightMiddle.svg";

const RightMiddle = () => {
  return (
    <Box position="absolute" right="0" bottom="55%" maxWidth="34%">
      <Box position="relative">
        <Stack marginLeft="37px" direction="row" alignItems="center">
          <Box flexShrink={0}>
            <FlagSVG />
          </Box>
          <Description marginBottom="10px" marginLeft="18px">
            {`When AR/VR technology is popular
            with the majority of users, RUNIE will
            transform itself into its own
            Metaverse (expected in 2024).`}
          </Description>
        </Stack>
        <Box position="absolute" bottom={0} sx={{ "*": { width: "100%" } }}>
          <RightMiddleSvg />
        </Box>
      </Box>
    </Box>
  );
};

const Description = styled(Typography)`
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 21px;

  color: #ffffff;
`;
export default RightMiddle;
