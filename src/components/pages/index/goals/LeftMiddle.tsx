import styled from "@emotion/styled";
import { Stack, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import TargetSVG from "components/svg/goals/Target.svg";
import React from "react";
import LeftMiddleSvg from "../../../svg/goals/Group 13LeftMiddle.svg";

const LeftMiddle = () => {
  return (
    <Box position="absolute" left="0" bottom="55%" maxWidth="28%">
      <Box position="relative">
        <Stack direction="row" alignItems="center" paddingBottom="20px">
          <Box flexShrink={0}>
            <TargetSVG />
          </Box>
          <Description marginLeft="18px">
            {`Not only making profits, RUNIE
            also helps to improve the
            health of users.`}
          </Description>
        </Stack>
        <Box position="absolute" bottom={0} sx={{ "*": { width: "100%" } }}>
          <LeftMiddleSvg />
        </Box>
      </Box>
    </Box>
  );
};

const Description = styled(Typography)`
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 21px;

  color: #ffffff;
`;
export default LeftMiddle;
