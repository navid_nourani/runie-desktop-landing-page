import { Stack, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import { styled } from "@mui/system";
import GroupSVG from "components/svg/goals/Group.svg";
import React from "react";
import RightTopSvg from "../../../svg/goals/Group 15LeftBottom.svg";
const LeftBottom = () => {
  return (
    <Box position="absolute" left="0" bottom="28%" maxWidth="34%">
      <Box position="relative">
        <Stack direction="row" alignItems="center" paddingBottom="15px">
          <Box flexShrink={0}>
            <GroupSVG />
          </Box>
          <Description marginLeft="18px">
            {`RUNIE is a community-oriented product,
            especially during the time of the raging
            Covid-19 (improving health and income).`}
          </Description>
        </Stack>
        <Box position="absolute" bottom={0} sx={{ "*": { width: "100%" } }}>
          <RightTopSvg />
        </Box>
      </Box>
    </Box>
  );
};

const Description = styled(Typography)`
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 21px;

  color: #ffffff;
`;

export default LeftBottom;
