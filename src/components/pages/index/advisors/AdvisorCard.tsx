import styled from "@emotion/styled";
import { Stack, Typography } from "@mui/material";
import { Box } from "@mui/system";
import Image from "components/atoms/Image";
import React from "react";

interface Props {
  image: string;
  name: string;
  position: string;
  desccription?: string;
  width?: string;
  height?: string;
  imageheight?: string;
}

const AdvisorCard = ({
  image,
  name,
  position,
  desccription,
  width,
  height,
  imageheight,
}: Props) => {
  return (
    <Wrapper width={width ?? "400px"} height={height ?? "509px"}>
      <Box
        width="100%"
        height={imageheight ?? "379px"}
        position="relative"
        overflow="hidden"
        borderRadius="25px"
        bgcolor="#cccccc"
      >
        {image && (
          <Image
            style={{ marginBottom: "16px" }}
            src={image}
            alt=""
            objectFit="cover"
            layout="fill"
          />
        )}
      </Box>
      <Name>{name}</Name>
      <Pos>{position}</Pos>
      <HoverContentWrapper>
        <Name color="primary.contrastText">{name}</Name>
        <Pos color="primary.contrastText" marginBottom="25px">
          {position}
        </Pos>
        <Description color="primary.contrastText">{desccription}</Description>
      </HoverContentWrapper>
    </Wrapper>
  );
};

const HoverContentWrapper = styled(Stack)`
  width: 100%;
  display: none;
  position: absolute;
  top: 0;
  background-color: #375a64;
  z-index: 2;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled(Stack)`
  height: fit-content;
  position: relative;
  overflow: hidden;

  background: #ffffff;
  box-shadow: 0px 6px 24px rgba(0, 0, 0, 0.15);
  border-radius: 32px;
  padding: 24px;
  align-items: center;
  &:hover {
    ${HoverContentWrapper} {
      display: flex;
    }
  }
  scroll-snap-type: x mandatory;
`;

const Name = styled(Typography)`
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 30px;
  margin-bottom: 5px;
`;

const Pos = styled(Typography)`
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 27px;
`;

const Description = styled(Typography)`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  text-align: center;
  width: 229px;
`;

export default AdvisorCard;
