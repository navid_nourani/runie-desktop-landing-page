import styled from "@emotion/styled";
import { Container } from "@mui/material";
import { Box } from "@mui/system";
import Road from "components/svg/roadmap/Roadmap.svg";
import RoadContinue from "components/svg/roadmap/RoadmapContinue.svg";
import React from "react";

const RoadmapRoad = () => {
  return (
    <>
      <RoadContainer>
        <Box width="115%" position="relative" height="100%">
          <RoadTerminalContainer left="-10%">
            <RoadContinue />
          </RoadTerminalContainer>
          <Road />
          <RoadTerminalContainer left="unset" right="-10%">
            <RoadContinue />
          </RoadTerminalContainer>
        </Box>
      </RoadContainer>
    </>
  );
};

const RoadContainer = styled(Container)`
  position: absolute;
  top: 50%;
  transform: translate(-5%, -50%);
  width: 100%;
  & > svg {
    width: 100%;
  }
`;

const RoadTerminalContainer = styled(Box)`
  position: absolute;
  bottom: 0%;
  & > svg {
    width: 100%;
  }
`;

export default RoadmapRoad;
