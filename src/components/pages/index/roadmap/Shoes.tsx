import styled from "@emotion/styled";
import { Box } from "@mui/system";
import NFTShow from "components/svg/NFTShoe.svg";
import React from "react";

const Shoes = () => {
  return (
    <>
      {" "}
      <ShoeContainer
        zIndex={100}
        position="relative"
        top="-130px"
        left="-80px"
        sx={{
          transform: "rotate(22deg)",
          "&>svg": { width: "100%", overflow: "inherit" },
        }}
      >
        <NFTShow />
      </ShoeContainer>
      <ShoeContainer
        bottom="-100px"
        right="150px"
        sx={{
          transform: "rotate(-32deg)",
          "&>svg": { width: "100%", overflow: "inherit" },
        }}
      >
        <NFTShow />
      </ShoeContainer>
    </>
  );
};

const ShoeContainer = styled(Box)`
  position: absolute;
  color: #c59b6d;
`;

export default Shoes;
