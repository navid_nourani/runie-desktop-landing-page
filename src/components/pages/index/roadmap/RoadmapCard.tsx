import styled from "@emotion/styled";
import { Stack, Typography } from "@mui/material";
import { Box } from "@mui/system";
import Arrow from "components/svg/roadmap/Arrow2.svg";
import React from "react";

interface Props {
  reverse?: boolean;
  description?: string;
  left?: string;
  right?: string;
}

const RoadmapCard = ({ reverse, description, left, right }: Props) => {
  return (
    <Stack
      //   left={left}
      //   right={right}
      //   position="absolute"
      paddingTop={reverse ? "310px" : undefined}
      paddingBottom={reverse ? undefined : "390px"}
      alignItems="center"
      flexDirection={reverse ? "column-reverse" : "column"}
      justifyContent="flex-end"
      //   sx={{ transform: "translateX(-20%)" }}
    >
      <Wrapper
        flexDirection={reverse ? "column-reverse" : "column"}
        display="flex"
      >
        <Header>
          <Date color="primary.contrastText">03 2022</Date>
        </Header>
        <Box padding="13px">
          <Description>{description}</Description>
        </Box>
      </Wrapper>
      <Box
        sx={{
          "&>svg": {
            width: "50px",
            transform: reverse ? "scale(-1)" : undefined,
            marginBottom: reverse ? "-5px" : undefined,
          },
        }}
      >
        <Arrow />
      </Box>
    </Stack>
  );
};

const Wrapper = styled(Box)`
  width: 272px;
  min-height: 243px;
  height: fit-content;

  background: #ffffff;
  border-radius: 10px;
  overflow: hidden;
`;

const Header = styled(Stack)`
  width: 272px;
  height: 49px;

  background: #c49a6c;
  box-shadow: 0px 4px 9px rgba(0, 0, 0, 0.14);
  align-items: center;
  justify-content: center;
`;

const Date = styled(Typography)`
  font-style: normal;
  font-weight: 600;
  font-size: 22px;
  line-height: 33px;
`;

const Description = styled(Typography)`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
`;

export default RoadmapCard;
