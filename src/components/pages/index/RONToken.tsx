import { Stack } from "@mui/material";
import { Box } from "@mui/system";
import Image from "components/atoms/Image";
import { Title } from "../../atoms/Title";

const RONToken = () => {
  return (
    <Stack
      width="100vw"
      height="100vh"
      alignItems={"center"}
      flex={1}
      sx={{ "&>*": { width: "100%" } }}
    >
      <Title color="primary" paddingBottom="40px" textAlign={"center"}>
        RON Token
      </Title>
      <Box sx={{ width: "100%", height: "700px", position: "relative" }}>
        <Image
          src="/images/Runietoken2.png"
          objectFit="contain"
          alt=""
          layout="fill"
          quality={100}
        />
      </Box>
    </Stack>
  );
};

export default RONToken;
