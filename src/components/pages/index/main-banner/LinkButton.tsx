import { Button } from "@mui/material";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { FunctionComponent } from "react";
interface Props {
  children: React.ReactNode;
  href: string;
  target?: React.HTMLAttributeAnchorTarget;
  disabled?: boolean;
}

const LinkButton: FunctionComponent<Props> = ({
  children,
  href,
  target,
  disabled,
}) => {
  const router = useRouter();

  if (disabled) {
    return (
      <Button
        disabled
        variant={router.pathname === href ? "outlined" : "contained"}
        sx={{
          backgroundColor: "white",
          color: "primary.main",
          borderRadius: "4px",
          fontWeight: "bold",
          ":disabled": {
            backgroundColor: "white",
          },
        }}
      >
        {children}
      </Button>
    );
  }

  return (
    <Link href={href}>
      <a target={target}>
        <Button
          variant={router.pathname === href ? "outlined" : "contained"}
          sx={{
            backgroundColor:
              router.pathname === href ? "primary.light" : "white",
            color: router.pathname === href ? "white" : "primary.main",
            ":hover": { backgroundColor: "#8b8b8b" },
            borderRadius: "4px",
            fontWeight: "bold",
          }}
        >
          {children}
        </Button>
      </a>
    </Link>
  );
};

export default LinkButton;
