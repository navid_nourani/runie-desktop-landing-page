import { Stack, styled, Typography } from "@mui/material";
import { Box } from "@mui/system";
import Image from "components/atoms/Image";
import React from "react";

const SecondStep = () => {
  return (
    <Stack alignItems="center">
      <Box position="relative" width="177px" height="192px" marginBottom="14px">
        <Image
          src="/images/Logo/Runner2.png"
          alt=""
          layout="fill"
          objectFit="contain"
        />
      </Box>
      <Description color="primary">
        Running outside with GPS or do exercise tasks to get reward token
      </Description>
    </Stack>
  );
};

const Description = styled(Typography)`
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  text-align: center;
  width: 324px;
`;

export default SecondStep;
