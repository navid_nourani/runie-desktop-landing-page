import { Stack, styled, Typography } from "@mui/material";
import { Box } from "@mui/system";
import Image from "components/atoms/Image";
import React from "react";

const SecondStep = () => {
  return (
    <Stack alignItems="center">
      <Box position="relative" width="191px" height="196px" marginBottom="12px">
        <Image
          src="/images/Logo/RUNIEtoken.png"
          alt=""
          layout="fill"
          objectFit="contain"
        />
      </Box>
      <Description color="primary">
        Swap reward tokens to stable coin to take profits{" "}
      </Description>
    </Stack>
  );
};

const Description = styled(Typography)`
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  text-align: center;
  width: 205px;
`;

export default SecondStep;
