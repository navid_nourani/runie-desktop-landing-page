import { Stack, styled, Typography } from "@mui/material";
import { Box } from "@mui/system";
import NFTShow from "components/svg/Mask group.svg";
import React from "react";

const FirstStep = () => {
  return (
    <Stack alignItems="center">
      <Box
        marginBottom="36px"
        color="#308b8a"
        sx={{ "&>svg": { width: "100%", overflow: "inherit" } }}
      >
        <NFTShow />
      </Box>
      <Description color="primary">
        You need to own NFT sneaker to be ready
      </Description>
    </Stack>
  );
};

const Description = styled(Typography)`
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  text-align: center;
  width: 171px;
`;

export default FirstStep;
