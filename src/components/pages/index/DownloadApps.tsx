import styled from "@emotion/styled";
import { Box, Container, Fade, Stack, Typography } from "@mui/material";
import AppleStore from "components/svg/download-apps/AppleBadge.svg";
import Background from "components/svg/download-apps/Background.svg";
import GoogleStore from "components/svg/download-apps/GoogleBadge.svg";
import Dots from "components/svg/download-apps/Ornament 16Dots.svg";
import Link from "next/link";
import React, { useState } from "react";
import ReactSign from "react-sign";
import LeftMobile from "./download-apps/LeftMobile";
import MiddleMobile from "./download-apps/MiddleMobile";
import RightMobile from "./download-apps/RightMobile";

type Props = {};

const DownloadApps = (props: Props) => {
  const [isInView, setIsInView] = useState(false);
  return (
    <Container
      sx={{
        overflow: "hidden",
        position: "relative",
        paddingTop: "121px",
        width: "100%",
      }}
    >
      <ReactSign onEnter={() => setIsInView(true)} />
      <Fade
        in={isInView}
        style={{ transformOrigin: "0 0 0" }}
        {...(isInView ? { timeout: 1000 } : {})}
      >
        <StyledBox
          overflow="hidden"
          paddingTop="88px"
          position="relative"
          px="60px"
          paddingBottom="80px"
        >
          <Box
            position="absolute"
            height={"120%"}
            sx={{
              "*": { height: "90%" },
            }}
          >
            <Background />
          </Box>
          <Stack>
            <Box mt="-50px" mb="55px">
              <Dots />
            </Box>
            <Title variant="h3">Download RUNIE App</Title>
            <Description>
              RUNIE app will be available on Android and IOS operating systems.
              This helps to connect with users on multiple platforms.
            </Description>

            <Stack direction="row">
              <Link
                href="https://play.google.com/store/search?q=runie&c=apps"
                passHref
              >
                <A target="_blank">
                  <Box
                    sx={{
                      "*": {
                        width: "100%",
                      },
                    }}
                  >
                    <GoogleStore />
                  </Box>
                </A>
              </Link>
              <Link
                href="https://www.apple.com/us/search/runie?src=globalnav"
                passHref
              >
                <A style={{ marginLeft: "13px" }} target="_blank">
                  <Box
                    sx={{
                      "*": {
                        width: "100%",
                      },
                    }}
                  >
                    <AppleStore />
                  </Box>
                </A>
              </Link>
            </Stack>
            <Typography
              variant="h5"
              color="primary.contrastText"
              textAlign="center"
            >
              Comming soon...
            </Typography>
          </Stack>
          <Stack direction="row" height="100%">
            <LeftMobile />
            <MiddleMobile />
            <RightMobile />
          </Stack>
        </StyledBox>
      </Fade>
    </Container>
  );
};

const StyledBox = styled(Box)`
  background: rgb(49, 139, 139);
  background: linear-gradient(
    344deg,
    rgba(49, 139, 139, 1) 0%,
    rgba(55, 90, 100, 1) 83%
  );
  border-radius: 30px;
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 36px;
`;

const Title = styled(Typography)`
  font-weight: 600;
  font-size: 35px;
  line-height: 52px;
  color: #ffffff;
  margin-bottom: 15px;
`;

const Description = styled(Typography)`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  color: #ffffff;
  margin-bottom: 60px;
`;

const A = styled("a")`
  cursor: pointer;
  z-index: 5;
`;

export default DownloadApps;
