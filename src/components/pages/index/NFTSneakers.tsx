import { Container, Grow, Stack } from "@mui/material";
import { Box, styled } from "@mui/system";
import React, { useState } from "react";
import ReactSign from "react-sign";
import { Title } from "../../atoms/Title";
import SneakerCard from "./nft-sneakers/SneakerCard";

const NFTSneakers = () => {
  const [isInView, setIsInView] = useState(false);
  return (
    <StyledContainer>
      <Title color="primary" marginBottom="60px">
        NFT Sneakers
      </Title>
      <ReactSign onEnter={() => setIsInView(true)} />
      <CardsContainer>
        <Grow
          in={isInView}
          style={{ transformOrigin: "0 0 0" }}
          {...(isInView ? { timeout: 1000 * 1 } : {})}
        >
          <Box>
            <SneakerCard image={"/images/NFTsneaker/Asset6@3x.png"} />
          </Box>
        </Grow>
        <Grow
          in={isInView}
          style={{ transformOrigin: "0 0 0" }}
          {...(isInView ? { timeout: 1000 * 3 } : {})}
        >
          <Box>
            <SneakerCard image={"/images/NFTsneaker/Asset7@3x.png"} />
          </Box>
        </Grow>
        <Grow
          in={isInView}
          style={{ transformOrigin: "0 0 0" }}
          {...(isInView ? { timeout: 1000 * 4 } : {})}
        >
          <Box>
            <SneakerCard image={"/images/NFTsneaker/Asset8@3x.png"} />
          </Box>
        </Grow>
        <Grow
          in={isInView}
          style={{ transformOrigin: "0 0 0" }}
          {...(isInView ? { timeout: 1000 * 5 } : {})}
        >
          <Box>
            <SneakerCard image={"/images/NFTsneaker/Asset9@3x.png"} />
          </Box>
        </Grow>
      </CardsContainer>
    </StyledContainer>
  );
};

const StyledContainer = styled(Container)`
  position: relative;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const CardsContainer = styled(Stack)`
  flex-direction: row;
  & > *:not(:first-of-type) {
    margin-left: 20px;
  }
  & > *:nth-of-type(2n-1) {
    margin-top: 70px;
  }
`;

export default NFTSneakers;
