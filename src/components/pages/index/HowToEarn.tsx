import { Container, Fade, Stack, Typography } from "@mui/material";
import { Box, styled } from "@mui/system";
import ArrowRight from "components/svg/how-to-learn/arrow/ArrowRight.svg";
import BottomLeftArrow from "components/svg/how-to-learn/arrow/BottomLeftArrow.svg";
import LeftTopArrow from "components/svg/how-to-learn/arrow/LeftTopArrow.svg";
import SpiralArrow from "components/svg/how-to-learn/arrow/SpiralArrow.svg";
import React, { useState } from "react";
import ReactSign from "react-sign";
import { Title } from "../../atoms/Title";
import FirstStep from "./how-to-earn/FirstStep";
import SecondStep from "./how-to-earn/SecondStep";
import ThirdStep from "./how-to-earn/ThirdStep";
const HowToEarn = () => {
  const [isInView, setIsInView] = useState(false);
  return (
    <Container
      sx={{
        height: "100vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Stack alignItems="center" width="100%">
        <Title marginBottom="48px" color="primary">
          HOW TO EARN?
        </Title>
        <ReactSign onEnter={() => setIsInView(true)} />
        <Stack width="100%">
          <Stack direction="row" width="100%" alignItems="center">
            <Fade
              in={isInView}
              style={{ transformOrigin: "0 0 0" }}
              {...(isInView ? { timeout: 1000 * 1 } : {})}
            >
              <Box>
                <FirstStep />
              </Box>
            </Fade>
            <Fade
              in={isInView}
              style={{ transformOrigin: "0 0 0" }}
              {...(isInView ? { timeout: 1000 * 2 } : {})}
            >
              <ArrowContainer>
                <SpiralArrow />
              </ArrowContainer>
            </Fade>
            <Fade
              in={isInView}
              style={{ transformOrigin: "0 0 0" }}
              {...(isInView ? { timeout: 1000 * 3 } : {})}
            >
              <Box>
                <SecondStep />
              </Box>
            </Fade>
            <Fade
              in={isInView}
              style={{ transformOrigin: "0 0 0" }}
              {...(isInView ? { timeout: 1000 * 4 } : {})}
            >
              <ArrowContainer>
                <ArrowRight />
              </ArrowContainer>
            </Fade>
            <Fade
              in={isInView}
              style={{ transformOrigin: "0 0 0" }}
              {...(isInView ? { timeout: 1000 * 5 } : {})}
            >
              <Box>
                <ThirdStep />
              </Box>
            </Fade>
          </Stack>
          <Stack
            direction="row"
            width="100%"
            justifyContent="center"
            marginTop="-30px"
          >
            <Fade
              in={isInView}
              style={{ transformOrigin: "0 0 0" }}
              {...(isInView ? { timeout: 1000 * 9 } : {})}
            >
              <Box>
                <LeftTopArrow />
              </Box>
            </Fade>
            <Fade
              in={isInView}
              style={{ transformOrigin: "0 0 0" }}
              {...(isInView ? { timeout: 1000 * 7 } : {})}
            >
              <ForthStep color="primary">
                Reinvest in burn mechanisms or apply other ways to improve
                profits (Upgrade NFT, staking, complete missions, running with
                friend, ...)
              </ForthStep>
            </Fade>
            <Fade
              in={isInView}
              style={{ transformOrigin: "0 0 0" }}
              {...(isInView ? { timeout: 1000 * 6 } : {})}
            >
              <Box display="flex" alignItems="flex-end" marginBottom="30px">
                <BottomLeftArrow />
              </Box>
            </Fade>
          </Stack>
        </Stack>
      </Stack>
    </Container>
  );
};

const ArrowContainer = styled(Box)({
  flex: 1,
  alignItems: "center",
  justifyContent: "center",
  display: "flex",
  marginBottom: "60px",
  "*": { width: "100%" },
});

const ForthStep = styled(Typography)`
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  text-align: center;
  width: 605px;
  align-self: flex-end;
  margin-bottom: 20px;
  padding-inline: 16px;
`;

export default HowToEarn;
