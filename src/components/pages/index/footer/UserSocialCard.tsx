import { Stack, styled, Typography } from "@mui/material";
import { Box, BoxProps } from "@mui/system";
import TelegramIcon from "components/svg/socials/Telegram.svg";
import Link from "next/link";
import React from "react";
import SocialRoundButton from "./SocialRoundButton";

type Props = {
  telegramId: string;
  url: string;
} & BoxProps;

const UserSocialCard = ({ url, telegramId, ...props }: Props) => {
  return (
    <Link href={url}>
      <a target={"__blank"}>
        <Wrapper {...props}>
          <Stack direction="row" alignItems={"center"}>
            <SocialRoundButton
              customColor="#C49A6C"
              icon={<TelegramIcon />}
              url={url}
            />
            <ID color="primary.contrastText">@{telegramId}</ID>
          </Stack>
        </Wrapper>
      </a>
    </Link>
  );
};

const Wrapper = styled(Box)`
  width: 291px;
  height: 50px;

  background: rgba(49, 139, 139, 0.33);
  border-radius: 25px;
`;

const ID = styled(Typography)`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  /* identical to box height */

  font-feature-settings: "liga" off;

  margin-left: 16px;
`;

export default UserSocialCard;
