import { Box, styled } from "@mui/system";
import Link from "next/link";
import React from "react";

interface Props {
  icon: JSX.Element;
  customColor?: string;
  marginLeft?: string;
  url: string;
}

const SocialRoundButton = ({ icon, customColor, marginLeft, url }: Props) => {
  return (
    <Box
      marginLeft={marginLeft}
      bgcolor={customColor ?? "#318B8B"}
      display="flex"
      alignItems="center"
      justifyContent="center"
      width="50px"
      height="50px"
      borderRadius="50%"
    >
      <Link href={url ?? "3S"} passHref>
        <A target="__blank">{icon} </A>
      </Link>
    </Box>
  );
};

const A = styled("a")`
  font-size: 0px;
`;

export default SocialRoundButton;
