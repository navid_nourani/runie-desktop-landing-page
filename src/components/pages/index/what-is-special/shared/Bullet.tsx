import { Box } from "@mui/system";
import InnerBullet from "components/svg/what-is-special/Bullet Inner.svg";
import OuterBullet from "components/svg/what-is-special/Bullet Outer.svg";
import React from "react";

type Props = {};

const Bullet = (props: Props) => {
  return (
    <Box position="relative" width="28px" height="28px">
      <Box
        position="absolute"
        width="100%"
        height="100%"
        top="50%"
        left="50%"
        sx={{ transform: "translate(-50%,-50%)" }}
      >
        <OuterBullet />
      </Box>
      <Box
        width="100%"
        height="100%"
        position="absolute"
        top="50%"
        left="50%"
        sx={{ transform: "translate(-47%,-33%)" }}
      >
        <InnerBullet />
      </Box>
    </Box>
  );
};

export default Bullet;
