import { Box } from "@mui/material";
import React from "react";
import Bullet from "../shared/Bullet";

const RightBullets = () => {
  return (
    <>
      <Box
        sx={{ transform: "unset" }}
        position="absolute"
        right="-13px"
        bottom="-5px"
        width="28px !important"
      >
        <Bullet />
      </Box>
      <Box position="absolute" width="28px !important" top="-3px" left="-12px">
        <Bullet />
      </Box>
    </>
  );
};

export default RightBullets;
