import { Box } from "@mui/material";
import React from "react";
import Bullet from "../shared/Bullet";

const LeftBullets = () => {
  return (
    <>
      <Box
        sx={{ transform: "unset" }}
        position="absolute"
        left="-13px"
        width="28px !important"
        bottom="-5px"
      >
        <Bullet />
      </Box>
      <Box position="absolute" width="28px !important" top="-3px" right="-12px">
        <Bullet />
      </Box>
    </>
  );
};

export default LeftBullets;
