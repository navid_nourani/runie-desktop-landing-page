import { Box, Stack } from "@mui/material";
import R1Connector from "components/svg/what-is-special/R1Connector.svg";
import R2Connector from "components/svg/what-is-special/R2Connector.svg";
import R3Connector from "components/svg/what-is-special/R3Connector.svg";
import R4Connector from "components/svg/what-is-special/R4Connector.svg";
import React from "react";
import LeftBullets from "./connectors/LeftBullets";
import RightBullets from "./connectors/RightBullets";
import Bullet from "./shared/Bullet";

const Connectors = () => {
  return (
    <Box
      px="9%"
      display="grid"
      position="relative"
      height="100%"
      gap="36px"
      width="100%"
    >
      <Stack direction="row" width="100%">
        <Box
          sx={{
            position: "relative",
            transform: "translateX(8%)",
            "*": { width: "100%", height: "fit-content" },
          }}
        >
          <Box
            sx={{ transform: "unset" }}
            position="absolute"
            left="-12px"
            width="28px !important"
            bottom="-16px"
          >
            <Bullet />
          </Box>
          <Box
            position="absolute"
            width="28px !important"
            top="-17px"
            right="-12px"
          >
            <Bullet />
          </Box>
          <R4Connector />
        </Box>
        <Box
          sx={{
            position: "relative",
            transform: "translateX(-28%)",
            "*": { width: "100%", height: "fit-content" },
          }}
        >
          <LeftBullets />
          <R3Connector />
        </Box>
        <Box
          sx={{
            transform: "translateX(22%)",
            "*": { width: "100%", height: "fit-content" },
          }}
        >
          <RightBullets />
          <R2Connector />
        </Box>
        <Box
          sx={{
            transform: "translateX(-7%)",
            "*": { width: "100%", height: "fit-content" },
          }}
        >
          <RightBullets />
          <R1Connector />
        </Box>
      </Stack>
    </Box>
  );
};

export default Connectors;
