import styled from "@emotion/styled";
import { Box, Typography } from "@mui/material";
import React from "react";

const TitleBox = () => {
  return (
    <Container>
      <Title color="primary" variant="h3">
        What makes RUNIE special?
      </Title>
    </Container>
  );
};

const Container = styled(Box)`
  width: 100%;
  max-width: 858px;
  min-height: 132px;

  display: flex;
  justify-content: center;
  align-items: center;
  background: #f3f3f3;
  border: 3px solid #318b8b;
  box-sizing: border-box;
  box-shadow: 0px 12px 21px rgba(0, 0, 0, 0.14);
  border-radius: 16px;
`;

const Title = styled(Typography)`
  font-weight: 700;
  font-size: 52px;
  line-height: 78px;
  /* identical to box height */

  text-transform: capitalize;
`;

export default TitleBox;
