import styled from "@emotion/styled";
import { Box } from "@mui/material";
import AllItems from "components/svg/roadmap/All Items.svg";
import React from "react";
import { Title } from "../../atoms/Title";
import ImageAnimator from "../../organism/ImageAnimator";
import Shoes from "./roadmap/Shoes";
const Roadmap = () => {
  return (
    <Wrapper>
      <Title color="primary.contrastText">Roadmap</Title>
      {/* <RoadmapRoad /> */}
      <Shoes />
      <ImageAnimator>
        <Box sx={{ "&>svg": { width: "100%" } }}>
          <AllItems />
        </Box>
      </ImageAnimator>
    </Wrapper>
  );
};

const Wrapper = styled(Box)`
  min-height: 100vh;
  display: flex;
  align-items: center;
  flex-direction: column;
  position: relative;
  padding-block: 41px;
  background: linear-gradient(111.18deg, #375a64 31.02%, #318b8b 86.16%);
`;

const calculateCardLeft = (index: number) => {
  return `calc(${(75 / 5) * index}%)`;
};

export default Roadmap;
