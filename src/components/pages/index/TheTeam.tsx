import { Stack } from "@mui/material";
import Grow from "@mui/material/Grow";
import { Box, styled } from "@mui/system";
import ArrowLeft from "components/svg/arrow-button/Arrow Left.svg";
import ArrowRight from "components/svg/arrow-button/Arrow Right.svg";
import React, { useRef, useState } from "react";
import ReactSign from "react-sign";
import { Title } from "../../atoms/Title";
import { teamMembers } from "../../__mocks__/team";
import AdvisorCard from "./advisors/AdvisorCard";

const TheTeam = () => {
  const [isInView, setIsInView] = useState(false);

  const scrollViewRef = useRef<HTMLDivElement>();
  const scrollRight = () => {
    if (!scrollViewRef.current) {
      return;
    }
    const correntScrollLeft = scrollViewRef.current.scrollLeft;
    const scrollWidth = scrollViewRef.current.scrollWidth;
    const clientWidth = scrollViewRef.current.clientWidth;
    const scrollTo = correntScrollLeft + clientWidth / 2;

    if (correntScrollLeft + clientWidth >= scrollWidth) {
      scrollViewRef.current.scrollLeft = 0;
    } else {
      scrollViewRef.current.scrollLeft = scrollTo;
    }
  };

  const scrollLeft = () => {
    if (!scrollViewRef.current) {
      return;
    }
    const correntScrollLeft = scrollViewRef.current.scrollLeft;
    const scrollWidth = scrollViewRef.current.scrollWidth;
    const clientWidth = scrollViewRef.current.clientWidth;
    const scrollTo = correntScrollLeft - clientWidth / 2;
    if (correntScrollLeft === 0) {
      scrollViewRef.current.scrollLeft = scrollWidth;
    } else {
      scrollViewRef.current.scrollLeft = scrollTo;
    }
  };

  return (
    <Stack
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Title color="primary">The Team</Title>

      <Box
        width="100%"
        py="60px"
        px="calc((100vw - 1239px)/2 - 24px)"
        ref={scrollViewRef}
        sx={{ overflowX: "scroll" }}
      >
        <ReactSign threshold={0.2} onEnter={() => setIsInView(true)}>
          <AdvisorsContainer px="24px">
            {teamMembers.map((member, index) => (
              <Grow
                key={`member-${member.name}`}
                in={isInView}
                style={{ transformOrigin: "0 0 0" }}
                {...(isInView ? { timeout: index * 1000 } : {})}
              >
                <Box width="295px" height="385px">
                  <AdvisorCard
                    width="295px"
                    height="385px"
                    imageheight="265px"
                    position={member.role}
                    image={member.image}
                    name={member.name}
                    desccription={member.description}
                  />
                </Box>
              </Grow>
            ))}
          </AdvisorsContainer>
        </ReactSign>
      </Box>
      <Stack direction="row">
        <StyledButton onClick={scrollLeft}>
          <ArrowLeft />
        </StyledButton>
        <StyledButton onClick={scrollRight} sx={{ marginLeft: "31px" }}>
          <ArrowRight />
        </StyledButton>
      </Stack>
    </Stack>
  );
};

const StyledButton = styled("button")`
  all: unset;
  cursor: pointer;
`;

const AdvisorsContainer = styled(Stack)`
  flex-direction: row;
  width: fit-content;
  & > *:not(:first-of-type) {
    margin-left: 22px;
  }
`;

export default TheTeam;
