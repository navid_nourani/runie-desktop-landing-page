import { Container, Stack, Typography } from "@mui/material";
import { Box } from "@mui/system";
import Image from "components/atoms/Image";
import DotsSVG from "components/svg/Ornament 35Dots.svg";
import PluseSVG from "components/svg/Ornament 59Plus.svg";
import React from "react";

const WhatIsRUNIE = () => {
  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent={"center"}
      height="100vh"
      width="100vw"
      pt="84px"
      pb="27px"
      maxHeight="100vh"
      position="relative"
      bgcolor="white"
      overflow="hidden"
    >
      <Box position="absolute" bottom="-25%" left="0">
        <PluseSVG />
      </Box>
      <Box position="absolute" top="0" right="0">
        <DotsSVG />
      </Box>
      <Container>
        <Box
          height="100%"
          display="grid"
          gridTemplateColumns={"1fr 1fr"}
          gap="48px"
        >
          <Stack alignSelf={"center"}>
            <Typography
              variant="h3"
              fontSize="3.25rem"
              fontWeight={700}
              marginBottom="20px"
            >
              What is RUNIE?
            </Typography>
            <Typography variant="body1" fontSize="1rem">
              {`RUNIE is a Web3 lifestyle app built on Binance Smart
                Chain (BSC) platform. Based on Move To Earn concept,
                RUNIE helps users to both improve their health through
                walking, jogging, climbing, etc ... while earning profits.
                By owning RUNIE's NFT sneaker, users will able to
                participate in this attractive earning mechanism.
            `}
            </Typography>
          </Stack>
          <Box component="div" position="relative" width="100%" height="600px">
            <Image
              src="/images/Logo/Runner.png"
              alt=""
              layout="fill"
              quality={100}
              objectFit="contain"
            />
          </Box>
        </Box>
      </Container>
    </Box>
  );
};

export default WhatIsRUNIE;
