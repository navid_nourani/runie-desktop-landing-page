import { Box, Stack, Typography } from "@mui/material";
import { styled } from "@mui/system";
import Image from "components/atoms/Image";
import RunnerSVG from "components/svg/goals/Running man.svg";
import ShoeSVG from "components/svg/goals/Shoe.svg";
import React from "react";

const Goals = () => {
  return (
    <Stack
      height={"100vh"}
      width="100vw"
      position="relative"
      bgcolor="#375A64"
      overflow="hidden"
    >
      <Title flex={1}>RUNIE’s Goal</Title>
      <Box position="relative" width="100%" height="1500px">
        <Image
          src="/images/Banner/goals.png"
          alt=""
          layout="fill"
          objectFit="contain"
        />
      </Box>
      <Box position="absolute" left="0" bottom="0">
        <RunnerSVG />
      </Box>{" "}
      <Box position="absolute" right="0" bottom="-20px">
        <ShoeSVG />
      </Box>
    </Stack>
  );
};

const Title = styled(Typography)`
  font-weight: 700;
  font-size: 52px;
  line-height: 78px;
  text-align: center;
  color: #fff;
  margin-top: 52px;
`;

export default Goals;

// import { Box, Stack, Typography } from "@mui/material";
// import { styled } from "@mui/system";
// import Image from "components/atoms/Image";
// import React from "react";
// import ImageAnimator from "../../../organism/ImageAnimator";

// const Goals = () => {
//   return (
//     <Stack flex={2}>
//       <Title>RUNIE’s Goal</Title>
//       <ImageAnimator>
//         <Box position="relative" width="100%" height="500px">
//           <Image
//             src="/images/Banner/goals.png"
//             alt=""
//             layout="fill"
//             objectFit="contain"
//           />
//         </Box>
//       </ImageAnimator>
//     </Stack>
//   );
// };

// const Title = styled(Typography)`
//   font-weight: 700;
//   font-size: 52px;
//   line-height: 78px;
//   text-align: center;
//   color: #fff;
//   margin-top: 52px;
// `;

// export default Goals;
