import { Container, Stack, Typography } from "@mui/material";
import { Box, styled } from "@mui/system";
import React from "react";
import ImageAnimator from "../../organism/ImageAnimator";
import Connectors from "./what-is-special/Connectors";
import TitleBox from "./what-is-special/TitleBox";

const WhatIsSpecial = () => {
  return (
    <Container
      sx={{
        width: "100%",
        minheight: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        position: "relative",
      }}
    >
      <ImageAnimator>
        <Stack alignItems={"center"} width="100%">
          <TitleBox />
          <Connectors />
          <Box
            display={"grid"}
            gridTemplateColumns="repeat(4, 1fr)"
            gap="24px"
            width="100%"
          >
            <DescriptionContainer>
              <DescriptionPaper>
                <Description color="primary">
                  RUNIE allows users to invite more friends to run with them to
                  increase rewards. This allows both the inviter and the invitee
                  to maximize their profits.
                </Description>
              </DescriptionPaper>
            </DescriptionContainer>
            <DescriptionContainer>
              <DescriptionPaper>
                <Description color="primary">
                  {`RUNIE's UI/UX is created by a highly skilled and experienced
                team. User experience is one of the things RUNIE focuses on
                most.`}
                </Description>
              </DescriptionPaper>
            </DescriptionContainer>
            <DescriptionContainer>
              <DescriptionPaper>
                <Description color="primary">
                  RUNIE has a mechanism that allows users to rent NFT sneakers.
                  This makes it easy for users to expand their revenue and also
                  make it easier for newcomers to access the app.
                </Description>
              </DescriptionPaper>
            </DescriptionContainer>
            <DescriptionContainer>
              <DescriptionPaper>
                <Description color="primary">
                  RUNIE provides users with courses, tasks are exercises from
                  light to heavy and even diet tips. Besides running, when
                  completing these tasks, users will also have rewards. RUNIE
                  helps users not only earn income but also gain knowledge about
                  health.
                </Description>
              </DescriptionPaper>
            </DescriptionContainer>
          </Box>
        </Stack>
      </ImageAnimator>
    </Container>
  );
};

const DescriptionContainer = styled(Box)`
  width: 100%;
  height: 100%;

  border: 2.5px solid #318b8b;
  box-sizing: border-box;
  border-radius: 16px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const DescriptionPaper = styled(Box)`
  width: 100%;
  height: 100%;

  background: #ffffff;
  box-shadow: 0px 6px 24px rgba(0, 0, 0, 0.15);
  border-radius: 12px;
  padding-block: 24px;
  padding-inline: 17px;
`;

const Description = styled(Typography)`
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
`;

export default WhatIsSpecial;
