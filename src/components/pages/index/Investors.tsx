import styled from "@emotion/styled";
import { Container, Fade, Stack } from "@mui/material";
import { Box } from "@mui/system";
import Partner1 from "components/svg/partners/partner1.svg";
import Partner2 from "components/svg/partners/partner2.svg";
import Partner3 from "components/svg/partners/partner3.svg";
import Partner4 from "components/svg/partners/partner4.svg";
import Partner5 from "components/svg/partners/partner5.svg";
import Partner6 from "components/svg/partners/partner6.svg";
import Partner7 from "components/svg/partners/partner7.svg";
import Partner8 from "components/svg/partners/partner8.svg";
import React, { useState } from "react";
import ReactSign from "react-sign";
import { Title } from "../../atoms/Title";

const Investors = () => {
  const [isInView, setIsInView] = useState(false);
  return (
    <Container>
      <Stack alignItems="center" marginBottom="221px">
        <Title marginBottom="48px" color="primary">
          {"Investor & Partner"}
        </Title>
        <ReactSign onEnter={() => setIsInView(true)} />
        <PartnersContainer
          direction="row"
          alignItems="center"
          justifyContent="center"
          gap="27px"
        >
          <Fade
            in={isInView}
            style={{ transformOrigin: "0 0 0" }}
            {...(isInView ? { timeout: 1000 * 1 } : {})}
          >
            <Box>
              <Partner1 />
            </Box>
          </Fade>
          <Fade
            in={isInView}
            style={{ transformOrigin: "0 0 0" }}
            {...(isInView ? { timeout: 1000 * 3 } : {})}
          >
            <Box>
              <Partner2 />
            </Box>
          </Fade>
          <Fade
            in={isInView}
            style={{ transformOrigin: "0 0 0" }}
            {...(isInView ? { timeout: 1000 * 3 } : {})}
          >
            <Box>
              <Partner3 />
            </Box>
          </Fade>
          <Fade
            in={isInView}
            style={{ transformOrigin: "0 0 0" }}
            {...(isInView ? { timeout: 1000 * 4 } : {})}
          >
            <Box>
              <Partner4 />
            </Box>
          </Fade>
          <Fade
            in={isInView}
            style={{ transformOrigin: "0 0 0" }}
            {...(isInView ? { timeout: 1000 * 5 } : {})}
          >
            <Box>
              <Partner5 />
            </Box>
          </Fade>
          <Fade
            in={isInView}
            style={{ transformOrigin: "0 0 0" }}
            {...(isInView ? { timeout: 1000 * 6 } : {})}
          >
            <Box>
              <Partner6 />
            </Box>
          </Fade>
          <Fade
            in={isInView}
            style={{ transformOrigin: "0 0 0" }}
            {...(isInView ? { timeout: 1000 * 7 } : {})}
          >
            <Box>
              <Partner7 />
            </Box>
          </Fade>
          <Fade
            in={isInView}
            style={{ transformOrigin: "0 0 0" }}
            {...(isInView ? { timeout: 1000 * 8 } : {})}
          >
            <Box>
              <Partner8 />
            </Box>
          </Fade>
        </PartnersContainer>
      </Stack>
    </Container>
  );
};

const PartnersContainer = styled(Stack)`
  direction: row;
  align-items: center;
  flex-wrap: wrap;
`;

export default Investors;
