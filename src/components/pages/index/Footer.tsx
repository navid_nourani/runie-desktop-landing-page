import { Container, Grid, Stack, Typography } from "@mui/material";
import { Box, styled } from "@mui/system";
import FacebookIcon from "components/svg/socials/Facebook.svg";
import TelegramIcon from "components/svg/socials/Telegram.svg";
import TwitterIcon from "components/svg/socials/Twitter.svg";
import UnknownIcon from "components/svg/socials/Unknown.svg";
import WebsiteIcon from "components/svg/socials/Website.svg";
import Image from "next/image";
import React from "react";
import Logo from "../../svg/Group 2Logo.svg";
import SocialRoundButton from "./footer/SocialRoundButton";
import UserSocialCard from "./footer/UserSocialCard";

const telegramIds = [
  "RUNIEGlobal",
  "RUNIEThailand",
  "RUNIEIndonesia",
  "RUNIEIndia",
  "RUNIESpain",
  "RUNIEBrazil",
  "RUNIERussia",
  "RUNIEPhilippines",
  "RUNIEArabic",
  "RUNIEBangladeshi",
];

const Footer = () => {
  return (
    <Wrapper bgcolor="primary">
      <Box
        position="absolute"
        bottom={"0"}
        right="40px"
        width="144px"
        height="192px"
        sx={{ "&": { transform: "scaleX(-1)" } }}
      >
        <Image
          src="/images/Running man 2@3x.png"
          alt=""
          unoptimized
          layout="fill"
        />
      </Box>
      <Container>
        <Stack>
          <Stack alignItems="center" direction="row" marginBottom="75px">
            <Box flex={1} sx={{ "&>svg": { height: "100%", width: "200px" } }}>
              <Logo />
            </Box>
            <SocialRoundButton
              marginLeft="26px"
              icon={<TwitterIcon />}
              url="https://twitter.com/RUNIEOfficial"
            />
            <SocialRoundButton
              marginLeft="26px"
              icon={<WebsiteIcon />}
              url="https://runie.life"
            />
            <SocialRoundButton
              marginLeft="26px"
              icon={<TelegramIcon />}
              url="https://t.me/RUNIEChannel"
            />
            <SocialRoundButton
              marginLeft="26px"
              icon={<FacebookIcon />}
              url="https://facebook.com/RUNIEOfficial"
            />
            <SocialRoundButton
              marginLeft="26px"
              icon={<UnknownIcon />}
              url="https://runie.medium.com/"
            />
          </Stack>
        </Stack>
        <Grid gap="23px" container marginBottom="66px" position="relative">
          {telegramIds.map((telegramId, index) => (
            <Grid item key={`telegram-id-${telegramId}`}>
              <UserSocialCard
                telegramId={telegramId}
                url={`https://t.me/${telegramId}`}
              />
            </Grid>
          ))}
        </Grid>
        <CopyRight
          color="primary.contrastText"
          alignSelf="flex-start"
          textAlign={"start"}
        >
          © 2022 RUNIE. All rights reserved
        </CopyRight>
      </Container>
    </Wrapper>
  );
};

const Wrapper = styled(Box)(({ theme }) => ({
  position: "relative",
  paddingTop: "62px",
  paddingBottom: "34px",
  backgroundColor: theme.palette.primary.main,
}));

const CopyRight = styled(Typography)`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  /* identical to box height */

  font-feature-settings: "liga" off;
`;

export default Footer;
