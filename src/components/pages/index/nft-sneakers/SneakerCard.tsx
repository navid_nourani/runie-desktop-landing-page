import { Stack } from "@mui/material";
import { Box, styled } from "@mui/system";
import Image from "components/atoms/Image";
import React from "react";

type Props = {
  image: string;
};

const SneakerCard = ({ image }: Props) => {
  return (
    <Box width="100%">
      <Wrapper>
        <Image src={image} alt="" layout="fill" objectFit="contain" />
      </Wrapper>
    </Box>
  );
};

const Wrapper = styled(Stack)`
  width: 460px;
  height: 580px;
  position: relative;
  margin: -39px;
`;

export default SneakerCard;
