import styled from "@emotion/styled";
import { Container, Stack, Typography } from "@mui/material";
import { Box } from "@mui/system";
import Image from "components/atoms/Image";
import React from "react";
import { Title } from "../../atoms/Title";
import ImageAnimator from "../../organism/ImageAnimator";

const FoundationStory = () => {
  return (
    <Container
      sx={{
        minHeight: "100vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Stack alignItems="center" width="100%">
        <Title color="primary" marginBottom="57px">
          Foundation Story
        </Title>
        <Box display="grid" gridTemplateColumns={"1fr 1fr"} gap="30px">
          <Stack>
            <Description color="primary">
              At the end of 2021, the idea of RUNIE was conceived by Edvard
              (Founder). With enthusiasm and common sense, Max (CMO), Zerker
              (Design) and Regina (CFO) joined the team in January 2022. Then
              the next members of the team join the big target RUNIE. To conquer
              these important milestones, our team has worked hard day and night
              to prepare to bring to the community a practical and meaningful
              product.
            </Description>
            <NoteBoxWrapper>
              <Qoute color="primary.dark">
                “Not just a simple product. RUNIE will contribute to the
                improvement of your life”
              </Qoute>
              <Signiture color="primary.dark">-RUNIE Team</Signiture>
            </NoteBoxWrapper>
          </Stack>
          <ImageAnimator>
            <Box height="451px" position="relative" maxWidth={"100%"}>
              <Image
                src="/images/ourTeam.png"
                alt=""
                layout="fill"
                objectFit="contain"
              />
            </Box>
          </ImageAnimator>
        </Box>
      </Stack>
    </Container>
  );
};

const Description = styled(Typography)`
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 30px;
  margin-bottom: 37px;
`;

const NoteBoxWrapper = styled(Box)`
  width: 612px;
  height: fit-content;
  padding: 26px;

  background-color: #ffffff;
  border-radius: 20px;
`;

const Qoute = styled(Typography)`
  font-style: italic;
  font-weight: 600;
  font-size: 20px;
  line-height: 30px;

  margin-bottom: 24px;
`;

const Signiture = styled(Typography)`
  font-style: italic;
  font-weight: 600;
  font-size: 20px;
  line-height: 30px;
`;

export default FoundationStory;
