import { Box, Slide, Stack, styled } from "@mui/material";
import ArrowLeft from "components/svg/arrow-button/Arrow Left.svg";
import ArrowRight from "components/svg/arrow-button/Arrow Right.svg";
import React, { useState } from "react";
import RONToken from "./RONToken";
import RunieToken from "./RunieToken";

const RONTokenData = () => {
  const [slideNumber, setSlideNumber] = useState(0);

  const handleNext = () => {
    if (slideNumber === 1) {
      setSlideNumber(0);
    } else {
      setSlideNumber(slideNumber + 1);
    }
  };
  const handlePrev = () => {
    if (slideNumber === 0) {
      setSlideNumber(1);
    } else {
      setSlideNumber(slideNumber - 1);
    }
  };
  return (
    <Box width="100vw" height="100vh" position="relative">
      <Slide direction="left" in={slideNumber === 0} mountOnEnter unmountOnExit>
        <Box width="100vw" height="100vw">
          <RunieToken />
        </Box>
      </Slide>
      <Slide direction="left" in={slideNumber === 1} mountOnEnter unmountOnExit>
        <Box width="100vw" height="100vw">
          <RONToken />
        </Box>
      </Slide>
      <Stack
        direction="row"
        position="absolute"
        bottom="60px"
        left="50%"
        sx={{ transform: "translateX(-50%)" }}
      >
        <StyledButton onClick={handlePrev}>
          <ArrowLeft />
        </StyledButton>
        <StyledButton onClick={handleNext} sx={{ marginLeft: "31px" }}>
          <ArrowRight />
        </StyledButton>
      </Stack>
    </Box>
  );
};

const StyledButton = styled("button")`
  all: unset;
  cursor: pointer;
`;

export default RONTokenData;
