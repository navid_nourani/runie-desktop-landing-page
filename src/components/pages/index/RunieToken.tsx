import { Stack } from "@mui/material";
import { Box } from "@mui/system";
import Image from "components/atoms/Image";
import React from "react";
import { Title } from "../../atoms/Title";

const RunieToken = () => {
  return (
    <Stack
      height="100vh"
      width="100vw"
      alignItems={"center"}
      flex={1}
      sx={{ "&>*": { width: "100%" } }}
    >
      <Title color="primary" paddingBottom="40px" textAlign={"center"}>
        RUNIE Token
      </Title>
      <Box position="relative" width="100%" height="700px">
        <Image
          src="/images/Runietoken.png"
          alt=""
          layout="fill"
          quality={100}
          objectFit="contain"
        />
      </Box>
    </Stack>
  );
};

export default RunieToken;
