import { Box, Container, Stack } from "@mui/material";
import Background from "components/svg/tokenomic/background.svg";
import Data from "components/svg/tokenomic/Data.svg";
import React from "react";
import { Title } from "../../atoms/Title";
import ImageAnimator from "../../organism/ImageAnimator";

const Tokenomic = () => {
  return (
    <Box
      position="relative"
      height="100%"
      py="95px"
      sx={{ "&>svg": { width: "100%", height: "100%" } }}
    >
      <Background />
      <Box
        position="absolute"
        top="0"
        display="flex"
        alignItems="center"
        justifyContent="center"
        width="100%"
        height="100%"
      >
        <Container
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Stack alignItems="center">
            <Title color="primary.contrastText">Tokenomic</Title>
            <ImageAnimator>
              <Box sx={{ "&>svg": { width: "100%" } }}>
                <Data />
              </Box>
            </ImageAnimator>
          </Stack>
        </Container>
      </Box>
    </Box>
  );
};

export default Tokenomic;
