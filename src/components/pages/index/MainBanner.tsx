import { Player } from "@lottiefiles/react-lottie-player";
import { Box, Container, Stack, styled } from "@mui/material";
import Image from "components/atoms/Image";
import Link from "next/link";
import React from "react";
import Logo from "../../svg/Logo.svg";
import LinkButton from "./main-banner/LinkButton";

const MainBanner = () => {
  return (
    <Box position="relative" height="100vh" width="100vw">
      <StyledImage
        src="/images/Banner/banner.png"
        alt=""
        quality={100}
        layout="fill"
        objectFit="cover"
      />
      <Container
        sx={{
          paddingTop: "40px",
          position: "relative",
          zIndex: 10,
          display: "flex",
          flexDirection: "row",
        }}
      >
        <Box sx={{ "&>*>svg": { width: "45px" } }}>
          <Link href="/" passHref>
            <A>
              <Logo />
            </A>
          </Link>
        </Box>

        <Stack
          direction="row"
          sx={{
            position: "absolute",
            top: "40px",
            left: "50%",
            transform: "translateX(-50%)",
            "& > *": {
              marginLeft: "20px !important",
            },
          }}
        >
          <LinkButton target="_blank" href="/RUNIE Whitepaper v1.2 (1).pdf">
            Whitepaper
          </LinkButton>
          <LinkButton href="/privateSale">Private Sale</LinkButton>
          <LinkButton disabled href="/presale">
            Presale
          </LinkButton>
        </Stack>
      </Container>
      <Box
        position="absolute"
        bottom="20px"
        right="20px"
        sx={{
          "&>*": {
            height: "72px",
            width: "72px",
            color: "white",
          },
        }}
      >
        <Player
          autoplay
          loop
          src="https://assets1.lottiefiles.com/packages/lf20_qonxes7r.json"
        ></Player>
      </Box>
    </Box>
  );
};

const A = styled("a")`
  position: "relative";
`;

const StyledImage = styled(Image)`
  z-index: -1;
  background-color: white;
`;

export default MainBanner;
