import { Container, Grow, Stack } from "@mui/material";
import { Box, styled } from "@mui/system";
import React, { useState } from "react";
import ReactSign from "react-sign";
import { Title } from "../../atoms/Title";
import AdvisorCard from "./advisors/AdvisorCard";

const Advisors = () => {
  const [isInView, setIsInView] = useState(false);
  return (
    <Container
      sx={{
        minHeight: "100vh",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Title color="primary" marginBottom="48px">
        The Advisors
      </Title>
      <ReactSign onEnter={() => setIsInView(true)} />
      <AdvisorsContainer>
        <Grow
          in={isInView}
          style={{ transformOrigin: "0 0 0" }}
          {...(isInView ? { timeout: 0 } : {})}
        >
          <Box width="400px" height="509px">
            <AdvisorCard
              position="Advisor"
              image="/images/teams/TIMGALE@3x.png"
              name="Tim Gale"
              desccription="11 years of experience in the blockchain
            field. 14 years of
            experience in investment and business."
            />
          </Box>
        </Grow>
        <Grow
          in={isInView}
          style={{ transformOrigin: "0 0 0" }}
          {...(isInView ? { timeout: 1000 } : {})}
        >
          <Box width="400px" height="509px">
            <AdvisorCard
              position="Advisor"
              image="/images/teams/BRANDEELAWSON@3x.png"
              name="Brandee Lawson"
              desccription="“Founder of MKF
            ventures. Former
            director of D&B Group.”"
            />{" "}
          </Box>
        </Grow>
        <Grow
          in={isInView}
          style={{ transformOrigin: "0 0 0" }}
          {...(isInView ? { timeout: 2000 } : {})}
        >
          <Box width="400px" height="509px">
            <AdvisorCard
              position="Advisor"
              image="/images/teams/WILLMORRIS@3x.png"
              name="Will Morris"
              desccription="“12 years of experience
          in the field of health
          and sports. 10 years
          working at FLG Group.”"
            />
          </Box>
        </Grow>
      </AdvisorsContainer>
    </Container>
  );
};

const AdvisorsContainer = styled(Stack)`
  flex-direction: row;
  & > *:not(:first-of-type) {
    margin-left: 22px;
  }
`;

export default Advisors;
