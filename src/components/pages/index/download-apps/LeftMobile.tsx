import { Box } from "@mui/system";
import LeftMobileSvg from "components/svg/download-apps/Left Phone.svg";
import LeftPhoneShadowSVG from "components/svg/download-apps/LeftPhoneShadow.svg";
import React from "react";

const LeftMobile = () => {
  return (
    <Box position="relative" maxHeight={"100%"}>
      <Box>
        <LeftMobileSvg />
      </Box>
      <Box
        sx={{
          transform: "translateX(30%)",
        }}
        position="absolute"
        bottom="-50px"
        left={0}
      >
        <LeftPhoneShadowSVG />
      </Box>
    </Box>
  );
};

export default LeftMobile;
