import { Box } from "@mui/system";
import RightPhoneShadowSVG from "components/svg/download-apps/LeftPhoneShadow.svg";
import RightMobileSvg from "components/svg/download-apps/Right Phone.svg";
import React from "react";

const RightMobile = () => {
  return (
    <Box position="relative" maxHeight={"100%"}>
      <Box paddingTop="30px">
        <RightMobileSvg />
      </Box>
      <Box
        sx={{ transform: "scaleX(-1) translateX(30px)" }}
        position="absolute"
        bottom="-50px"
        right={0}
      >
        <RightPhoneShadowSVG />
      </Box>
    </Box>
  );
};

export default RightMobile;
