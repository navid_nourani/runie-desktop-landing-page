import { Box } from "@mui/system";
import MiddleMobileSvg from "components/svg/download-apps/Middle Phone.svg";
import MiddlePhoneShadowSVG from "components/svg/download-apps/MiddlePhoneShadow.svg";
import React from "react";

const MiddleMobile = () => {
  return (
    <Box position="relative" maxHeight={"100%"}>
      <Box paddingTop="30px">
        <MiddleMobileSvg />
      </Box>
      <Box
        sx={{
          transform: "translateX(10%)",
        }}
        position="absolute"
        bottom="-30px"
        left={0}
      >
        <MiddlePhoneShadowSVG />
      </Box>
    </Box>
  );
};

export default MiddleMobile;
