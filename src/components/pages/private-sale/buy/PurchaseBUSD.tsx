import styled from "@emotion/styled";
import { Stack, StackProps, TextField, Typography } from "@mui/material";
import { useAtom } from "jotai";
import React, { useState } from "react";
import { bnbAmountAtom } from "../../../../stores/pages/priateSale";
import Actions from "./purchase-busd/Actions";

type props = StackProps & { bnbMinBuy?: number };

const PurchaseBUSD: React.FunctionComponent<props> = ({
  bnbMinBuy,
  ...props
}) => {
  const [busdAmount, setBusdAmount] = useAtom(bnbAmountAtom);
  const [busdIsLow, setBusdIsLow] = useState(false);

  const busdChangeHanlder = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (Number(e.target.value) < 0) {
      return;
    }
    setBusdAmount(e.target.value);
    if (!bnbMinBuy) {
      setBusdIsLow(false);
    } else if (e.target.value && Number(e.target.value) < bnbMinBuy) {
      setBusdIsLow(true);
    } else {
      setBusdIsLow(false);
    }
  };

  return (
    <>
      <Wrapper {...props}>
        <AddressTitle>BNB: </AddressTitle>
        <StyledInput
          variant="standard"
          value={busdAmount}
          error={busdIsLow}
          type="number"
          helperText={busdIsLow ? `Minimum buy: ${bnbMinBuy}BNB ` : undefined}
          onChange={busdChangeHanlder}
        />
        <Actions BusdAmount={busdAmount} bnbMinBuy={bnbMinBuy} />
      </Wrapper>
    </>
  );
};

const AddressTitle = styled(Typography)`
  font-weight: 400;
  font-size: 15px;
  line-height: 22px;
  /* identical to box height */

  text-align: center;
`;

const StyledInput = styled(TextField)`
  border-radius: 35px;
  margin-left: 8px;
`;

const Wrapper = styled(Stack)`
  flex-direction: row;
  align-items: center;
  & > * {
    margin-left: 8px;
  }
`;

export default PurchaseBUSD;
