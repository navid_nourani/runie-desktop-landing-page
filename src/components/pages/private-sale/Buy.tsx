import { Stack, styled, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { Divider } from "components/atoms/Divider";
import Text from "components/atoms/Text";
import FormContainer from "components/organism/FormContainer";
import { useAtomValue } from "jotai";
import Link from "next/link";
import React, { FunctionComponent } from "react";
import { bnbAmountAtom, bnbPriceAtom } from "../../../stores/pages/priateSale";
import PurchaseBUSD from "./buy/PurchaseBUSD";

interface Props {
  title: string;
  bnbMinBuy?: number;
  runiePriceInDollars: number;
  allocation: string;
  hardCap: string;
  decimalNumberOfDigits?: number;
  duration: string;
  closeTime: string;
  distributionDate: string;
  closeDescription: string;
  runieInBnbPrice: string;
}

const Buy: FunctionComponent<Props> = ({
  title,
  bnbMinBuy,
  runiePriceInDollars,
  allocation,
  hardCap,
  decimalNumberOfDigits,
  duration,
  closeTime,
  distributionDate,
  closeDescription,
  runieInBnbPrice,
}) => {
  const busdAmount = useAtomValue(bnbAmountAtom);
  const bnbPrice = useAtomValue(bnbPriceAtom);

  return (
    <FormContainer
      alignItems={"center"}
      display="flex"
      flexDirection={"column"}
      title={title}
      marginBottom="5px"
      paddingBottom="16px !important"
    >
      <PurchaseBUSD marginBottom="8px" bnbMinBuy={bnbMinBuy} />
      <Stack direction="row" alignItems="center" sx={{ marginBottom: "3px" }}>
        <Text>Estimate:</Text>
        <Box width="70px">
          <Text textAlign={"center"}>
            {(Number(busdAmount) / Number(runieInBnbPrice)).toFixed(0)}
          </Text>
        </Box>
        <Text>RUNIE</Text>
        <Divider sx={{ marginInline: "10px", height: "21px" }} />
        <Text>
          *Min buy:{" "}
          {bnbMinBuy
            ? `${bnbMinBuy} BNB (~$
          ${((bnbMinBuy * Number(bnbPrice)) / 100).toFixed(2)})`
            : "No"}
        </Text>
      </Stack>
      <Box marginBottom="3px">
        <Text>Close time: {closeTime}</Text>
      </Box>
      <BottomBoxWrapper>
        <Text>First Come First Serve</Text>
        <Text>{closeDescription}</Text>
        <Text>Duration: {duration}</Text>
        <Text>
          Price: 1 RUNIE = {runieInBnbPrice} BNB (~${runiePriceInDollars} /
          RUNIE)
        </Text>
        <Text>
          Allocation: {allocation} RUNIE | Hard cap: {hardCap} BNB
        </Text>
        <Text>Distribution date: {distributionDate}</Text>
      </BottomBoxWrapper>
      <Link href="https://bscscan.com/address/0xf32923150E59a3680fd6233D7B22A21E1503e298">
        <a target="_blank">
          <Typography>
            <A>View RUNIE contract</A>
          </Typography>
        </a>
      </Link>
    </FormContainer>
  );
};

const A = styled("a")`
  font-family: "Secular One";
  font-style: normal;
  text-decoration: underline;
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  text-align: center;
  cursor: pointer;
`;

const BottomBoxWrapper = styled(Stack)`
  border: 1px solid #fffbfb;
  box-sizing: border-box;
  filter: drop-shadow(0px 10px 10px rgba(0, 0, 0, 0.25));
  border-radius: 35px;
  width: 100%;
  padding: 7px;
  align-items: center;
  margin-bottom: 6px;
`;

export default Buy;
