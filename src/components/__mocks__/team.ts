interface ITeamMember {
  name: string;
  role: string;
  description: string;
  image: string;
}

export const teamMembers: ITeamMember[] = [
  {
    name: "EDVARD DOBROMILA",
    role: "Founder - CEO",
    description: `7 years of experience in the blockchain field. Graduated MBA from Durham University. Won the second prize of Samsung - SCPC 2015 contest.`,
    image: "/images/teams/EDVARDDOBROMILA@3x.png",
  },
  {
    name: "MARCELA JOSEF",
    role: "CTO",
    description:
      "12 years of programming experience. Won MaxTech Award 2011. Participated in more than 10 software released on Google Store.",
    image: "/images/teams/MARCELAJOSEF@3x.png",
  },
  {
    name: "MAX VLASTISLAV",
    role: "CMO",
    description:
      "8 years of marketing experience. 4 years of experience in the blockchain field. Former CMO Minor Group.",
    image: "/images/teams/MAXVLASTISLAV@3x.png",
  },
  {
    name: "REGINA TATIANA",
    role: "CFO",
    description:
      "10 years of financial management experience. 3 years of fund management experience.",
    image: "/images/teams/REGINATATIANA@3x.png",
  },
  {
    name: "JERKER OSCAR",
    role: "Lead Designer",
    description:
      "9 years of graphic design experience. Freelance artist since the age of 13. Twice opened exhibitions in Sweden.",
    image: "/images/teams/JERKEROSCAR@3x.png",
  },
  {
    name: "ERNA ALBERT",
    role: "UI/UX designer",
    description:
      "5 years UI/UX design experience. Won third prize of Talent Designer 2018 contest in Sweden.",
    image: "/images/teams/ERNAALBERT@3x.png",
  },
  {
    name: "JAREK BERNARD",
    role: "Backend Developer",
    description:
      "6 years of programming experience at Arva Tech. 4 years of    blockchain programming experience.",
    image: "/images/teams/JAREKBERNARD@3x.png",
  },
  {
    name: "ZORKA FLANDA",
    role: "Smart Contract Developer",
    description:
      "5 years of experience in the blockchain field. 3 years of experience in smart contracts.",
    image: "/images/teams/ZORKAFLANDA@3x.png",
  },
  {
    name: "LAURA STONE",
    role: "Business Development",
    description:
      "3 years of sales experience. 5 years of experience in organizing promotional campaigns.",
    image: "/images/teams/LAURASTONE@3x.png",
  },
  {
    name: "GADAR ANUSH",
    role: "Community Manager",
    description:
      "3 years of experience in building and developing blockchain project communities. 2 years of community management experience at Axie Infinity.",
    image: "/images/teams/GADARANUSH@3x.png",
  },
  {
    name: "IBEN KEVIN",
    role: "Frontend Developer",
    description: "4 years of programming. High skill in Fullstack position.",
    image: "/images/teams/IBENKEVIN@3x.png",
  },
  {
    name: "EMILY DINA",
    role: "Marketing Executive",
    description:
      "5 years experience in blockchain marketing. 2 years experience in Digital Marketing. 3 years experience in Graphic Design.",
    image: "/images/teams/EMILYDINA@3x.png",
  },
];
