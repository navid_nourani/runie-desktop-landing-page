import { Stack } from "@mui/material";
import { Box } from "@mui/system";
import { Divider } from "components/atoms/Divider";
import Text from "components/atoms/Text";
import FormContainer from "components/organism/FormContainer";
import { useAtomValue } from "jotai";
import React from "react";
import { airDropDataAtom } from "../../stores/airdrop";
import WalletAddress from "./airdrop-result/WalletAddress";

const AirdropResult = () => {
  const airdropData = useAtomValue(airDropDataAtom);
  return (
    <FormContainer title="Airdrop Result" marginBottom="29px">
      <WalletAddress marginBottom="19px" />
      <Box display="grid" gridTemplateColumns="5fr 2px 4fr" gap="10px">
        <Stack alignItems="flex-start">
          <Text>
            Total entry point: {airdropData && airdropData.Total_Entries}
          </Text>
          <Stack width="100%" direction="row">
            <Text>Estimate reward token:</Text>
            <Box flex={1}>
              <Text textAlign={"center"}>
                {airdropData
                  ? (Number(airdropData.Total_Entries) * 9.6).toFixed(2)
                  : "--"}
              </Text>
            </Box>
            <Text>RUNIE</Text>
          </Stack>
        </Stack>
        <Divider />
        <Stack alignItems="flex-start">
          <Text>Distribution date: June 10th 2022</Text>
          <Text>Whitelist: {airdropData && airdropData.Whitelist}</Text>
        </Stack>
      </Box>
    </FormContainer>
  );
};

export default AirdropResult;
