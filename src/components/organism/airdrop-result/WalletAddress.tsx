import styled from "@emotion/styled";
import {
  Button,
  Stack,
  StackProps,
  TextField,
  Typography,
} from "@mui/material";
import { useUpdateAtom } from "jotai/utils";
import React from "react";
import web3 from "web3";
import { airDropDataAtom } from "../../../stores/airdrop";

const WalletAddress: React.FunctionComponent<StackProps> = (props) => {
  const [walletAddress, setWalletAddress] = React.useState("");
  const [isValidAddress, setIsValidAddress] = React.useState(false);
  const setAirdropData = useUpdateAtom(airDropDataAtom);

  const handleCheck = async () => {
    const data = await fetch(
      `/api/getAirdropDetail?walletAddress=${walletAddress}`
    );
    const res = await data.json();
    setAirdropData(res);
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setWalletAddress(e.target.value);
    setIsValidAddress(web3.utils.isAddress(e.target.value));
  };

  return (
    <Wrapper {...props}>
      <AddressTitle>Wallet address: </AddressTitle>
      <StyledInput
        variant="standard"
        error={!isValidAddress && walletAddress.length > 0}
        helperText={
          walletAddress.length > 0
            ? !isValidAddress && "Wallet address is invalid"
            : undefined
        }
        value={walletAddress}
        onChange={handleChange}
      />
      <Button
        disabled={!isValidAddress}
        onClick={handleCheck}
        variant="contained"
        color="warning"
        sx={{ marginLeft: "8px" }}
      >
        Check
      </Button>
    </Wrapper>
  );
};

const AddressTitle = styled(Typography)`
  font-weight: 400;
  font-size: 15px;
  line-height: 22px;
  /* identical to box height */

  text-align: center;
`;

const StyledInput = styled(TextField)`
  border-radius: 35px;
  margin-left: 8px;
  flex: 1;
`;

const Wrapper = styled(Stack)`
  flex-direction: row;
  align-items: center;
  & > * {
    margin-left: 8px;
  }
`;

export default WalletAddress;
