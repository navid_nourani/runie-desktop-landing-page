import styled from "@emotion/styled";
import { Typography } from "@mui/material";
import { Box, BoxProps } from "@mui/system";
import React from "react";

type Props = {
  children?: React.ReactNode;
  title: string;
  marginBottom?: string;
} & BoxProps;

const FormContainer: React.FunctionComponent<Props> = ({
  title,
  children,
  marginBottom,
  ...props
}) => {
  return (
    <Box position="relative" width="579px" marginBottom={marginBottom}>
      <WrapperTitle bgcolor="secondary.main">
        <Title color="secondary.contrastText">{title}</Title>
      </WrapperTitle>
      <ContentBox {...props}>{children}</ContentBox>
    </Box>
  );
};

const WrapperTitle = styled(Box)`
  border: 3px solid #fffbfb;
  box-sizing: border-box;
  box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.25);
  border-radius: 35px;
  width: 100%;
  height: 100%;
  position: absolute;
`;

const ContentBox = styled(Box)`
  background: radial-gradient(
      103.37% 1768.02% at 97.15% 50%,
      #375a64 51.82%,
      #318b8b 89.76%
    )
    /* warning: gradient uses a rotation that is not supported by CSS and may not behave as expected */;
  border: 3px solid #fffbfb;
  box-sizing: border-box;
  box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.25);
  border-radius: 35px;
  padding: 30px;
  position: relative;
  top: 40px;
  margin-bottom: 40px;
`;

const Title = styled(Typography)`
  font-weight: 400;
  font-size: 20px;
  line-height: 29px;
  /* identical to box height */

  text-align: center;
  padding: 8px;
`;

export default FormContainer;
