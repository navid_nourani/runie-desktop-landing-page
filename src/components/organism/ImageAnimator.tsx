import { Grow } from "@mui/material";
import { Box } from "@mui/system";
import React, { ReactNode, useState } from "react";
import ReactSign from "react-sign";

interface Props {
  children: ReactNode;
}

const ImageAnimator = ({ children }: Props) => {
  const [isInView, setIsInView] = useState(false);

  return (
    <>
      <ReactSign onEnter={() => setIsInView(true)}>
        <Grow
          in={isInView}
          style={{ transformOrigin: "0 0 0" }}
          {...(isInView ? { timeout: 1000 * 1 } : {})}
        >
          <Box width="100%" height="100%">
            {children}
          </Box>
        </Grow>
      </ReactSign>
    </>
  );
};

export default ImageAnimator;
