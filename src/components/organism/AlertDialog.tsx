import { DialogContent, Stack } from "@mui/material";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import * as React from "react";

const emails = ["username@gmail.com", "user02@gmail.com"];

export interface AlertDialogProps {
  title: string;
  open: boolean;
  onAccept: () => void;
  onClose: () => void;
  acceptText: string;
  cancelText: string;
  text: string;
}

function AlertDialog(props: AlertDialogProps) {
  const { onAccept, onClose, acceptText, cancelText, open, title, text } =
    props;

  const handleClose = () => {
    onClose();
  };

  const handleAccept = () => {
    onAccept();
    onClose();
  };

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle sx={{ color: "primary.main" }}>{title}</DialogTitle>
      <DialogContent>
        <Stack width="100%">
          {text}
          <Stack
            direction="row"
            width="100%"
            justifyContent={"center"}
            marginTop="24px"
          >
            <Button color="error" onClick={onClose}>
              {cancelText}
            </Button>
            <Button color="success" onClick={handleAccept}>
              {acceptText}
            </Button>
          </Stack>
        </Stack>
      </DialogContent>
    </Dialog>
  );
}

export default AlertDialog;
